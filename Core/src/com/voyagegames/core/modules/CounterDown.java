package com.voyagegames.core.modules;


public class CounterDown extends Counter {

	@Override
	final public void up() {
		// no-op
	}

	@Override
	final public void up(final int count) {
		// no-op
	}

}

package com.voyagegames.core.modules;

public class CounterUp extends Counter {

	@Override
	final public void down() {
		// no-op
	}

	@Override
	final public void down(final int count) {
		// no-op
	}

}

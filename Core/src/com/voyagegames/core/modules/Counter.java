package com.voyagegames.core.modules;

import com.voyagegames.core.interfaces.ICounter;

public class Counter implements ICounter {
	
	private int mValue;

	@Override
	public void up() {
		mValue++;
	}

	@Override
	public void up(int count) {
		mValue += count;
	}

	@Override
	public void down() {
		mValue--;
	}

	@Override
	public void down(final int count) {
		mValue -= count;
	}

	@Override
	public int value() {
		return mValue;
	}

}

package com.voyagegames.core.interfaces;

public interface ICounter {

	// Increments the counter value by one
	void up();
	
	// Increments the counter value by the specified amount
	void up(int count);

	// Decrements the counter value by one
	void down();
	
	// Decrements the counter value by the specified amount
	void down(int count);
	
	// Gets the counter value
	int value();
	
}

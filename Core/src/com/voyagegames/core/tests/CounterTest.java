package com.voyagegames.core.tests;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

import com.voyagegames.core.modules.Counter;

public class CounterTest {

	@Test
	public void value_initializedAtZero() {
		Counter c = new Counter();
		assertTrue(c.value() == 0);
	}

	@Test
	public void up_incrementsValue() {
		Counter c = new Counter();
		c.up();
		assertTrue(c.value() == 1);
	}

	@Test
	public void up_bySpecificValueIncrements() {
		Counter c = new Counter();
		c.up(5);
		assertTrue(c.value() == 5);
	}

	@Test
	public void down_decrementsValue() {
		Counter c = new Counter();
		c.down();
		assertTrue(c.value() == -1);
	}

	@Test
	public void down_decrementsBySpecificValue() {
		Counter c = new Counter();
		c.down(5);
		assertTrue(c.value() == -5);
	}

}

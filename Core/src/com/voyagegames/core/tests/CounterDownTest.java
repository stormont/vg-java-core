package com.voyagegames.core.tests;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

import com.voyagegames.core.modules.CounterDown;

public class CounterDownTest {

	@Test
	public void value_initializedAtZero() {
		CounterDown c = new CounterDown();
		assertTrue(c.value() == 0);
	}

	@Test
	public void up_isNoOp() {
		CounterDown c = new CounterDown();
		c.up();
		assertTrue(c.value() == 0);
	}

	@Test
	public void up_bySpecificValueIsNoOp() {
		CounterDown c = new CounterDown();
		c.up(5);
		assertTrue(c.value() == 0);
	}

	@Test
	public void down_decrementsValue() {
		CounterDown c = new CounterDown();
		c.down();
		assertTrue(c.value() == -1);
	}

	@Test
	public void down_decrementsBySpecificValue() {
		CounterDown c = new CounterDown();
		c.down(5);
		assertTrue(c.value() == -5);
	}

}

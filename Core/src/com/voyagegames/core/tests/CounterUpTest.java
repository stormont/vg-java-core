package com.voyagegames.core.tests;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

import com.voyagegames.core.modules.CounterUp;

public class CounterUpTest {

	@Test
	public void value_initializedAtZero() {
		CounterUp c = new CounterUp();
		assertTrue(c.value() == 0);
	}

	@Test
	public void up_incrementsValue() {
		CounterUp c = new CounterUp();
		c.up();
		assertTrue(c.value() == 1);
	}

	@Test
	public void up_bySpecificValueIncrements() {
		CounterUp c = new CounterUp();
		c.up(5);
		assertTrue(c.value() == 5);
	}

	@Test
	public void down_isNoOp() {
		CounterUp c = new CounterUp();
		c.down();
		assertTrue(c.value() == 0);
	}

	@Test
	public void down_bySpecificValueIsNoOp() {
		CounterUp c = new CounterUp();
		c.down(5);
		assertTrue(c.value() == 0);
	}

}
